import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class ovningar {
    public static void main(String[] args) {

        //1

        String[] names = {"Anton" , "Sofia" , "Elsa"};
        int[] numbers = {2, 4, 14 ,124};
        // 2
        //I Arrays så börjar indexeringen på 0 så
        // så det gör att Oslo hamnar på index 0,
        //New York på index 1, Prag på index 2 osv...
        String[] cities = {"Oslo", "New York" ,"Prag" , "Kyoto"};

        //3
        int newArray = numbers.length;

        int[] copyOfNumber = Arrays.copyOf(numbers, newArray);

        String[] copyOfNames = Arrays.stream(names).toArray(String[]::new);

        //4
        //Anledningen till att vi inte kan använda "==" operatorn är pga
        //att vi inte jämför det som finns inuti arrayerna utan arrayen i sig.
        int arr1[] = { 24, 10, 32 };
        int arr2[] = { 24, 10, 32 };


        if (Arrays.equals(arr1, arr2)) {
            System.out.println("Arrays are the same");
        }
        else {
            System.out.println("Arrays are not the same");
        }
        dice();
        specificNumbers();
    }

    //5
    //här skapar jag metoden dice
    public static int[] dice(){
        //här initerar jag arrayen
        int[] numbers = new int[101];
        //Här skapar jag ett Random klass object
        Random randNum = new Random();

        //En for-loop som körs så länge i är mindre än 101
        for (int i = 0; i < 101; i++) {
            //här sätter jag i till ett random generat nummer mellan 1 och 7
            numbers[i] = randNum.nextInt(1,7);
        }
        //här skriver jag ut min array där jag omvandlar den till strings
        System.out.println(Arrays.toString(numbers));
        //returnerar arrayen
        return numbers;

    }

    //del 2

    public static void specificNumbers(){

        int[] numbers = new int[101];

        Random randNum = new Random();


        int two = 0;
        int seven= 0;
        for (int i = 0; i < 101; i++) {

            numbers[i] = randNum.nextInt(0,10);
            if (numbers[i] == 2 ){
                two++;

            }else if (numbers[i] == 7){
                seven++;

            }
        }
        System.out.println("Number 2 appears " + two + " times");
        System.out.println("Number 7 appears " + seven + " times");


    }
}
